package config

// untuk melakukan konfigurasi MySQL

// error code #1 username = `user1` dan password= `1234567890`
// GRANT ALL PRIVILEGES ON db_apigo.* TO `user1`@`localhost`;
// FLUSH PRIVILEGES;

// username = `root`
// password = ``

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql" // untuk menghubungkan mysql dengan golang. gunain >> go get -u `url`
)

const (
	username = `root`
	password = ``
	database = `db_apigo`
)

var dsn = fmt.Sprintf(`%s:%s@/%s`, username, password, database)

// menghubungkan ke mysql
func Mysql() (db *sql.DB, err error) {
	db, err = sql.Open(`mysql`, dsn) // ngga bisa pake := why?
	// karena di parameter pake pointer ke db? >> (*sql.DB, error) jadi tidak deklarasi ulang

	if err != nil {
		// return nil, err
		fmt.Println(err)
	}

	// return db, nil
	return
}
